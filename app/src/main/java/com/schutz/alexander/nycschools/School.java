package com.schutz.alexander.nycschools;

import com.google.gson.annotations.SerializedName;

public class School {
    @SerializedName("school_name")
    private String schoolName;
    private String location;
    private String dbn;

    public String getSchoolName() { return schoolName; }
    public String getLocation() { return location; }
    public String getDbn() { return dbn; }
}
