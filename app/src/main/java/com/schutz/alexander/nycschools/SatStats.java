package com.schutz.alexander.nycschools;

import com.google.gson.annotations.SerializedName;

public class SatStats {
    @SerializedName("num_of_sat_test_takers")
    private String testTakers;
    @SerializedName("sat_critical_reading_avg_score")
    private String readingScore;
    @SerializedName("sat_math_avg_score")
    private String mathScore;
    @SerializedName("sat_writing_avg_score")
    private String writingScore;

    public String getTestTakers() { return testTakers; }
    public String getReadingScore() { return readingScore; }
    public String getMathScore() { return mathScore; }
    public String getWritingScore() { return writingScore; }
}
