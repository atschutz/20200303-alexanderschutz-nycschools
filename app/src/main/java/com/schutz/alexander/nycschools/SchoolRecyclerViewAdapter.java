package com.schutz.alexander.nycschools;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolRecyclerViewAdapter extends RecyclerView.Adapter<SchoolRecyclerViewAdapter.SchoolViewHolder> {
    private static final String TAG = "SRVAdapter";

    private List<String> titleList;
    private List<String> subtextList;
    private List<String> idList;

    private JsonApi jsonApi;

    public SchoolRecyclerViewAdapter(JsonApi jsonApi) {
        titleList = new ArrayList<>();
        subtextList = new ArrayList<>();
        idList = new ArrayList<>();

        this.jsonApi = jsonApi;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_list_item, viewGroup, false);

        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder schoolViewHolder, int i) {
        // Ensure we don't get any leftovers from the previously bound view.
        schoolViewHolder.addtlInfo.setVisibility(View.GONE);
        schoolViewHolder.resetSatFields();

        schoolViewHolder.title.setText(titleList.get(i));
        schoolViewHolder.subtext.setText(subtextList.get(i));

        schoolViewHolder.parent.setOnClickListener(v -> {
            Log.d(TAG, "onClick: clicked on " + titleList.get(i));

            Call<List<SatStats>> satCall = jsonApi.getScores(idList.get(i));
            satCall.enqueue(new Callback<List<SatStats>>() {
                @Override
                public void onResponse(Call<List<SatStats>> call, Response<List<SatStats>> response) {
                    if (!response.isSuccessful() || response.body() == null || response.body().isEmpty()) {
                        // If our callback doesn't return what we want, log the code;
                        Log.d(TAG, "onResponse: " + response.code());

                        // No data, set all fields to "No data available."
                        // I know I should be using string resources here and would
                        // usually do that.
                        schoolViewHolder.testTakers.setText("# of Test Takers: No data available");
                        schoolViewHolder.readingScore.setText("Avg. Reading Score: No data available");
                        schoolViewHolder.mathScore.setText("Avg. Math Score: No data available");
                        schoolViewHolder.writingScore.setText("Avg. Writing Score: No data available");

                    } else {
                        Log.d(TAG, "onResponse: dbn: " + idList.get(i));

                        // Set text for each field based on response.
                        // Generally I'd do this in a fancier way, with separate text views
                        // for the labels and the data, which would also mean I wouldn't have
                        // to rebuild the label every time, but I'm under a time limit.
                        SatStats satStats = response.body().get(0);

                        schoolViewHolder.testTakers.setText("# of Test Takers: " + satStats.getTestTakers());
                        schoolViewHolder.readingScore.setText("Avg. Reading Score: " + satStats.getReadingScore());
                        schoolViewHolder.mathScore.setText("Avg. Math Score: " + satStats.getMathScore());
                        schoolViewHolder.writingScore.setText("Avg. Writing Score: " + satStats.getWritingScore());
                    }
                }

                @Override
                public void onFailure(Call<List<SatStats>> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                }
            });

            // Set the visibility of the layout that displays SAT info. I considered doing this by
            // swapping in a fragment, but opted for this for the sake of time.
            // I'd also add some sort of sliding down animation, as well as a tint change on click.
            schoolViewHolder.addtlInfo.setVisibility(
                    schoolViewHolder.addtlInfo.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

        });
    }

    @Override
    public int getItemCount() {
        return titleList.size();
    }

    public static class SchoolViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout parent;
        public TextView title;
        public TextView subtext;
        public LinearLayout addtlInfo;
        public TextView testTakers;
        public TextView readingScore;
        public TextView mathScore;
        public TextView writingScore;

        public SchoolViewHolder(LinearLayout parent) {
            super(parent);

            // Bind all of our views here.
            this.parent = parent;
            this.title = parent.findViewById(R.id.title);
            this.subtext = parent.findViewById(R.id.subtext);
            this.addtlInfo = parent.findViewById(R.id.sat_scores);
            this.testTakers = parent.findViewById(R.id.test_takers);
            this.readingScore = parent.findViewById(R.id.reading_score);
            this.mathScore = parent.findViewById(R.id.math_score);
            this.writingScore = parent.findViewById(R.id.writing_score);
        }

        /**
         * Sets all fields in the SAT Info layout back to their default.
         */
        public void resetSatFields() {
            // Again, the labels should all be their own TextViews or at the very least
            // using strings.xml, but time is a factor.
            testTakers.setText("# of Test Takers:");
            readingScore.setText("Avg. Reading Score:");
            mathScore.setText("Avg. Math Score:");
            writingScore.setText("Avg. Writing Score:");
        }
    }

    /**
     * Adds another index to this adapter's data set.
     *
     * @param title String to be added to titleList.
     * @param subtext String to be added to subtextList.
     * @param id String to be added to idList.
     */
    public void add(String title, String subtext, String id) {
        titleList.add(title);
        subtextList.add(subtext);
        idList.add(id);

        // Adapter must be notified that data has changed or it will not reflect changes.
        //TODO move this out of add()
        notifyDataSetChanged();
    }
}
