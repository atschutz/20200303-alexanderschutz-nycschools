package com.schutz.alexander.nycschools;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    // I like to add a TAG to every class for the sake of debugging.
    // You'll see me pass them to logs as parameters.
    private static final String TAG = "MainActivity";

    private RecyclerView recyclerView;
    private JsonApi jsonApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // I'm using Retrofit and Gson for HTTP API to Java interfacing.
        // This is what I'm most used to.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonApi = retrofit.create(JsonApi.class);

        recyclerView = findViewById(R.id.recycler_view);

        // recyclerView will be taking up the whole screen, so its size will not change and we
        // can fix its size for optimization.
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // Pass our jsonApi member to the adapter so we can use it as part of the on click listener.
        RecyclerView.Adapter adapter = new SchoolRecyclerViewAdapter(jsonApi);
        recyclerView.setAdapter(adapter);

        initializeSchoolNameList();
    }

    /**
     * Calls to the HTTP endpoint ands sets the appropriate attributes for each school within the
     * recycler view.
     */
    private void initializeSchoolNameList() {
        Log.d(TAG, "initializeSchoolNameList: Populating school list");

        // Given more time, I'd like to add some sort of loading animation or splash screen to
        // occupy the UI thread while this thing gets the info it needs.
        Call<List<School>> schoolCall = jsonApi.getSchools();
        schoolCall.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                if (!response.isSuccessful()) {
                    // If our callback is not successful, log the code;
                    Log.d(TAG, "onResponse: " + response.code());
                } else if (response.body() != null) {
                    Log.d(TAG, "onResponse: Successful!");
                    for (School school : response.body()) {
                        // If we have a school name for this post, add to list.
                        if (school.getSchoolName() != null &&
                            !(school.getSchoolName().trim()).equals("") &&
                            recyclerView.getAdapter() != null) {
                                // Note that we add dbn to the parameter set so that we can use it
                                // to query SAT scores on click.
                                ((SchoolRecyclerViewAdapter)recyclerView.getAdapter())
                                        .add(school.getSchoolName(), school.getLocation(), school.getDbn());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                // If the callback fails, log the throw.
                Log.d(TAG, "onFailure: " + t);
            }
        });
    }
}
